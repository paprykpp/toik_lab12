package com.example.demo.service;

import com.example.demo.dto.SudokuDto;

public interface SudokuService {

    String PATH = "src/main/resources/sudoku.csv";
    SudokuDto boardVerification();
}
