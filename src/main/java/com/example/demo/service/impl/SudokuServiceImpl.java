package com.example.demo.service.impl;

import com.example.demo.dto.SudokuDto;
import com.example.demo.service.SudokuService;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;

public class SudokuServiceImpl implements SudokuService {

    @Override
    public SudokuDto boardVerification() {

        ArrayList<Integer> lineList = new ArrayList<>();
        ArrayList<Integer> columnList = new ArrayList<>();
        ArrayList<Integer> areaList = new ArrayList<>();
        int lineNumber;
        int columnNumber;
        int areaNumber;
        int[][] field = csvReader();
        int[][] tableArea = {
                {field[0][0], field[0][1], field[0][2], field[1][0], field[1][1], field[1][2], field[2][0], field[2][1], field[2][2]},
                {field[0][3], field[0][4], field[0][5], field[1][3], field[1][4], field[1][5], field[2][3], field[2][4], field[2][5]},
                {field[0][6], field[0][7], field[0][8], field[1][6], field[1][7], field[1][8], field[2][6], field[2][7], field[2][8]},
                {field[3][0], field[3][1], field[3][2], field[4][0], field[4][1], field[4][2], field[5][0], field[5][1], field[5][2]},
                {field[3][3], field[3][4], field[3][5], field[4][3], field[4][4], field[4][5], field[5][3], field[5][4], field[5][5]},
                {field[3][6], field[3][7], field[3][8], field[4][6], field[4][7], field[4][8], field[5][6], field[5][7], field[5][8]},
                {field[6][0], field[6][1], field[6][2], field[7][0], field[7][1], field[7][2], field[8][0], field[8][1], field[8][2]},
                {field[6][3], field[6][4], field[6][5], field[7][3], field[7][4], field[7][5], field[8][3], field[8][4], field[8][5]},
                {field[6][6], field[6][7], field[6][8], field[7][6], field[7][7], field[7][8], field[8][6], field[8][7], field[8][8]},
        };

        for(int i = 0; i < 9; i++){
            for(int j = 0; j < 9; j++){
                areaNumber = tableArea[i][j];
                lineNumber = field[i][j];
                columnNumber = field[j][i];
                for(int x = 0; x < 9; x++){
                    if(areaNumber == tableArea[i][x] && x != j){
                        areaList.add(i + 1);
                    }
                    if(lineNumber == field[i][x] && x != j){
                        lineList.add(i + 1);
                    }
                    if(columnNumber == field[x][i] && x != j){
                        columnList.add(i + 1);
                    }
                }
            }
        }

        LinkedHashSet<Integer> hasLine = new LinkedHashSet<>(lineList);
        LinkedHashSet<Integer> hasColumn = new LinkedHashSet<>(columnList);
        LinkedHashSet<Integer> hasArea = new LinkedHashSet<>(areaList);
        lineList = new ArrayList<>(hasLine);
        columnList = new ArrayList<>(hasColumn);
        areaList = new ArrayList<>(hasArea);
        return new SudokuDto(lineList,columnList,areaList);
    }

    private static int[][] csvReader(){
        int[][] field = new int[9][9];
        try {
            BufferedReader reader = new BufferedReader(new FileReader(PATH));
            String line;
            reader.readLine();
            int x = 0;
            while ((line = reader.readLine()) != null) {
                String[] cell = line.split(",");
                for (int y = 0; y < 9; y++) {
                    field[x][y] = Integer.parseInt(cell[y]);
                }
                x++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return field;
    }

}